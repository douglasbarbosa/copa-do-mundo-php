<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once './../class/selecao.php';

$dadosSelecao = Selecao::listarGruposSelecoes();

echo(json_encode($dadosSelecao));