<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once './../class/selecao.php';

$dadosGet = isset($_GET['idGrupo']) ? $_GET['idGrupo'] : null;

$dadosGrupo = Selecao::listarGrupos($dadosGet);

echo(json_encode($dadosGrupo));