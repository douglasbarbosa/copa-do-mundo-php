<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once './../class/selecao.php';

$dadosGet = isset($_GET['idSelecao']) ? $_GET['idSelecao'] : null;

$dadosSelecao = Selecao::listarSelecoes($dadosGet);

echo(json_encode($dadosSelecao));