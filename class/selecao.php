<?php

include_once './../config/database.php';

class Selecao{

	public $id;
	public $nome;
	public $sigla;

	public function getId() : string {
		return $this->id;
	}

	public function setId(string $id){
		$this->id = $nivel;
	}

	public function getNome() : string {
		return $this->nome;
	}

	public function setNome(string $nome){
		$this->nome = $nome;
	}

	public function getSigla() : string {
		return $this->sigla;
	}

	public function setSigla(string $sigla){
		$this->sigla = $sigla;
	}

	public function listarSelecoes($idSelecao){
		$db = DB::conectar();
		$comando = $db->prepare("CALL listar_selecoes(:idSelecao);");
		if($idSelecao){
			$comando->bindValue(":idSelecao", $idSelecao, PDO::PARAM_INT);
		}
		else{
			$comando->bindValue(":idSelecao", null, PDO::PARAM_NULL);
		}
		$comando->execute();
		$selecoes = $comando->fetchAll();

		return $selecoes;
	}

	public function listarGrupos($idGrupo){
		$db = DB::conectar();
		$comando = $db->prepare("CALL listar_grupos(:idGrupo);");
		if($idGrupo){
			$comando->bindValue('idGrupo', $idGrupo, PDO::PARAM_INT);
		}
		else{
			$comando->bindValue('idGrupo', null, PDO::PARAM_NULL);
		}
		$comando->execute();
		$grupos = $comando->fetchAll();

		return $grupos;
	}

	public function listarGruposSelecoes(){
		$db = DB::conectar();
		$comando = $db->prepare("CALL listar_grupos_selecoes();");
		$comando->execute();
		$gruposSelecoes = $comando->fetchAll();

		return $gruposSelecoes;
	}
}