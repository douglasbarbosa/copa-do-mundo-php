<?php
/**
 * Classe para acesso centralizado aos bancos de dados;
 * @version  1 Versão inicial
 */
abstract class DB {

	/**
	 * Conexão com o Banco de Dados.
	 * @var PDO
	 */
	public static $connection = null;
	/**
	 * Configurações padrão para conecção com o banco de dados.
	 * @var string[]
	 */
	public static $defaults = array(
		'driver' => 'mysql',
		'database' => 'copa_do_mundo',
		'host' => 'localhost',
		'user' => 'root',
		'password' => '',
		'options' => array(
			\PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
			\PDO::ATTR_ORACLE_NULLS       => \PDO::NULL_EMPTY_STRING,
			\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
		)
	);

	public static function fechar() {
		self::$connection = null;
	}

	public static function &conectar(array $config = null) : \PDO {
		$force = isset($config['force']) && $config['force'];
		if (self::$connection == null || $force) {
			if(self::$connection != null && $force) {
				self::fechar();
			}
			if (!is_array($config)) {
				if (is_null($config)) {
					$config = self::$defaults;
				} else {
					throw new InvalidArgumentException('A configuração deve ser passada por um array');
				}
			}
			if ($config != self::$defaults) {
				foreach (self::$defaults as $key => $value) {
					if (!array_key_exists($key, $config)) {
						$config[$key] = $value;
					} else if (is_array($config[$key]) && is_array($value)) {
						foreach ($value as $index => $content) {
							if (!array_key_exists($index, $config[$key])) {
								$config[$key][$index] = $content;
							}
						}
					}
				}
			}

			self::$connection = new \PDO(
				"{$config['driver']}:dbname={$config['database']};host={$config['host']};charset=utf8",
				$config['user'],
				$config['password'],
				$config['options']
			);
		}

		return self::$connection;
	}
}